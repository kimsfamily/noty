﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Notify
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public System.Windows.Forms.NotifyIcon Notify;
        public Button HideButton;
        public Queue<DateTime> TimeQ = new Queue<DateTime>();
        public bool IsRunning = false;

        public MainWindow()
        {
            InitializeComponent();

            Notify = new System.Windows.Forms.NotifyIcon();
//            Notify.Icon = Properties.Resources.Icon;
            Notify.Text = "김태양알리미";
            Notify.DoubleClick += Notify_DoubleClick;

            DisableBt2();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
//            if (SharedKeyword.isRunning == true)
            {
                Notify.Visible = true;
                this.Visibility = Visibility.Collapsed;
                e.Cancel = true;
                return;

            }
        }

        private void Notify_DoubleClick(object sender, EventArgs e)
        {
            Notify.Visible = false;
            this.Visibility = Visibility.Visible;
        }

        public DateTime StartDateTime;
        public DispatcherTimer Timer;

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            Timer = new DispatcherTimer();
            Timer.Interval = TimeSpan.FromMilliseconds(1);
            Timer.Tick += Dt_Tick;
        }

        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);
        private void Dt_Tick(object sender, EventArgs e)
        {
            var t = DateTime.Now - StartDateTime;
            TimeLabel.Content = this.Title = $"{t.Hours:00}:{t.Minutes:00}:{t.Seconds:00}:{t.Milliseconds/10:00}";// Timer $"{increment / 3600:00}:{increment / 60:00}:{increment%60:00}";

            if((int)t.TotalSeconds % 10 == 0)
                SetForegroundWindow(new WindowInteropHelper(Application.Current.MainWindow).Handle);
        }

        private void Bt1_Click(object sender, RoutedEventArgs e)
        {
            if (!IsRunning)
            {
                StartDateTime = DateTime.Now;
                Timer.Start();
                Bt1.Content = "PAUSE";

                EnableBt2();
                IsRunning = true;
            }
            else
            {
                Bt1.Content = "RESUME";
                Timer.Stop();
                IsRunning = false;
            }
        }

        private void Bt2_Click(object sender, RoutedEventArgs e)
        {
            Timer.Stop();
            Bt1.Content = "START";

            DisableBt2();
        }

        public void DisableBt2()
        {
            Bt2.Visibility = Visibility.Hidden;
            BtGrid.Children.Remove(Bt1);
            BtGrid.ColumnDefinitions.Clear();
            var col = new ColumnDefinition();
            BtGrid.ColumnDefinitions.Add(col);
            BtGrid.Children.Add(Bt1);
        }

        public void EnableBt2()
        {
            Bt2.Visibility = Visibility.Visible;
            BtCol.Width = GridLength.Auto;
            BtGrid.Children.Remove(Bt2);
            var col = new ColumnDefinition();
            BtGrid.ColumnDefinitions.Add(col);
            BtGrid.Children.Add(Bt2);
        }
    }
}
